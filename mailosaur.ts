import { expect } from "@playwright/test";
import MailosaurClient from "mailosaur";

const mailosaur = new MailosaurClient("KV0ObExH4yc43oxLT8LzjxhTIdy05r0o");

export async function getMessage(testStart: Date, userEmail) {
    const email = await mailosaur.messages.get(
        "dnqjcvjb",
        {
            sentTo: userEmail,
        },
        {
            timeout: 10000,
            receivedAfter: testStart,
        },
    );

    expect(email.html?.body).toContain("A new insider list has been created");
    expect(email.subject).toBe("InsiderLog Demo account");
}
