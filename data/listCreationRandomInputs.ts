import { faker } from "@faker-js/faker";
import moment from "moment";

export type List = {
    listTitle: string;
    listCreatedDate: string;
    listCreatedHour: string;
    reminderFrequency: string;
    aditionalNotes: string;
    clientName: string;
    corpRegNo: string;
    subAdmin: string;
    responsible: string;
    transactionType: string;
    industry: string;
    insideInfoDisclosedMS: string;
    issuerDescription: string;
    dateDisclosure: string;
    hourDisclosure: string;
};

const listName = () => {
    return "Automation Project" + moment().unix() + Math.round(Math.random() * 10);
};

const clientName = () => {
    return "Euronext" + moment().unix() + Math.round(Math.random() * 10);
};

const corpRegNo = () => {
    return "EN" + moment().unix() + Math.round(Math.random() * 10);
};

const randomPastDate = () => {
    return moment(faker.date.recent({ days: 10 })).format("YYYY-MM-DD");
};

const randomFutureDate = () => {
    return moment(faker.date.soon({ days: 5 })).format("YYYY-MM-DD");
};

const randomHour = () => {
    const randomMinutes = Math.floor(Math.random() * 60);
    const randomHour = Math.floor(Math.random() * 23);
    return `${randomHour.toString().padStart(2, "0")}:${randomMinutes.toString().padStart(2, "0")}`;
};

const reminderFrequency = () => {
    return String(faker.number.int(5));
};

const randomText = () => {
    return faker.lorem.text();
};

const randomDescription = () => {
    return "This person works as " + faker.person.jobTitle();
};

const randomEmail = () => {
    return (
        faker.person.firstName() +
        faker.color.human() +
        "@insiderlog.mailosaur.net"
    );
};

const randomName = () => {
    return faker.person.firstName() + ' aka "The ' + faker.animal.type() + '"';
};

const pickRandomItem = <T>(arr: T[]): T => {
    const randomIndex = Math.floor(Math.random() * arr.length);
    return arr[randomIndex];
};

const transactionTypeOptions = [
    "IPO",
    "Block trade",
    "Securities issue",
    "Private placement",
    "Other",
];
const industryOptions = [
    "Materials",
    "Oil and gas",
    "Industrials",
    "Consumer goods",
    "Consumer services",
    "Confidential",
    "Other",
];
const yesNo = ["Yes", "No"];

export const createRandomList = (): List => {
    return {
        listTitle: listName(),
        listCreatedDate: randomPastDate(),
        listCreatedHour: randomHour(),
        reminderFrequency: reminderFrequency(),
        aditionalNotes: randomText(),
        clientName: clientName(),
        corpRegNo: corpRegNo(),
        subAdmin: randomEmail(),
        responsible: randomName(),
        transactionType: pickRandomItem(transactionTypeOptions),
        industry: pickRandomItem(industryOptions),
        insideInfoDisclosedMS: pickRandomItem(yesNo),
        issuerDescription: randomDescription(),
        dateDisclosure: randomFutureDate(),
        hourDisclosure: randomHour(),
    };
};
