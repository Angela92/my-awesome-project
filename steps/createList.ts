import { Page, expect } from "@playwright/test";
import { CreateListForm } from "../pages/components/forms/createListForm";
import { List } from "../data/listCreationRandomInputs";
import { listConfigs } from "../pages/components/forms/createListForm";
import { ListIdPage } from "../pages/listIdPage";
import { ListPage } from "../pages/listPage";

export class CreateListSteps {
    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async userNavigatesToListPage(listName: string) {
        const listPage = new ListPage(this.page);

        await listPage.goto(listName);
    }

    async userCreatesAList(
        listConfig: listConfigs,
        listName: string,
        newListInfo: List,
    ) {
        const listPage = new ListPage(this.page);
        const createListForm = new CreateListForm(this.page);

        await listPage.newListBtn.click()
        await createListForm.createList(listConfig, listName, newListInfo);
    }

    async expectListToBeCreatedWithCorrectInfo(
        listInfo: List,
        listName: string,
    ) {
        const listIdPage = new ListIdPage(this.page);

        await expect(listIdPage.headerTitle).toContainText(listInfo.listTitle, {
            ignoreCase: true,
        });
        await listIdPage.isListInfoVisible(listInfo, listName);
    }
}
