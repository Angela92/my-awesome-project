import { expect, Locator, Page } from "@playwright/test";

export class LoginPage {
    readonly page: Page;
    readonly usernameInput: Locator;
    readonly passwordInput: Locator;
    readonly otpInput: Locator;
    readonly loginBtn: Locator;
    readonly alert: Locator;
    readonly forgotPass: Locator;
    readonly email: Locator;
    readonly confirmBtn: Locator;

    constructor(page: Page) {
        this.page = page;
        this.usernameInput = page.getByRole("textbox", {
            name: "Enter User Name/ Email",
        });
        this.passwordInput = page.getByRole("textbox", {
            name: "Enter your password",
        });
        this.otpInput = page.getByRole("textbox", {
            name: "Enter sms pincode",
        });
        this.loginBtn = page.getByRole("button", { name: "Login" });
        this.alert = page.getByRole("alert");
        this.forgotPass = page.getByRole("link", {
            name: "I forgot my password",
        });
        this.email = page.getByRole("textbox", { name: "Enter email" });
        this.confirmBtn = page.getByRole("button", { name: "Confirm" });
    }

    async goto() {
        await this.page.goto("/");
    }

    async login(page: Page, user: string, pass: string, otp: string) {
        await this.usernameInput.fill(user);
        await this.passwordInput.fill(pass);
        await this.loginBtn.click();
        await this.otpInput.fill(otp);
        await this.loginBtn.click();
        await expect(page).toHaveURL("/home");
    }

    async wrongLogin(user: string, pass: string) {
        await this.usernameInput.fill(user);
        await this.passwordInput.fill(pass);
        await this.loginBtn.click();
    }

    async forgotPassword(email: string) {
        await this.forgotPass.click();
        await this.email.fill(email);
        await this.confirmBtn.click();
    }
}
