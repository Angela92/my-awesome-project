import { Page, Locator, expect } from "@playwright/test";
import { TableComponent } from "playwright-components";
import { List } from "../data/listCreationRandomInputs";

export interface TableEntity extends Record<string, string> {
    listTitle: string;
    responsible: string;
    created: string;
    lastUpdated: string;
    persons: string;
    subAdmins: string;
}

export class ListIdPage {
    readonly headerTitle: Locator;
    readonly infoTable: TableComponent;

    constructor(page: Page) {
        this.headerTitle = page.locator("app-list-header");
        this.infoTable = new TableComponent(page.locator("info-table"));
    }

    async isListInfoVisible(
        listInfo: List,
        listName: string,
    ): Promise<boolean> {
        const entities: TableEntity[] = await this.infoTable.getBodyRowsAs();
        console.log("len: ", entities.length);
        console.info(entities);

        expect(entities[0].listTitle).toContain(listInfo.listTitle);
        expect(entities[0].created).toContain(listInfo.listCreatedDate);
        expect(entities[0].created).toContain(listInfo.listCreatedHour);
        listName === "MARKET_SOUNDING"
            ? expect(entities[0].responsible).toContain("QA Auto 1 QA Auto 1")
            : expect(entities[0].responsible).toContain(listInfo.responsible);

        return true;
    }
}
