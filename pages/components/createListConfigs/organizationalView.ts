import { Page, Locator } from "@playwright/test";
import { List } from "../../../data/listCreationRandomInputs";

export class OrganizationalView {
    readonly page: Page;
    readonly client: Locator;
    readonly corpRegNo: Locator;
    readonly projectTitle: Locator;
    readonly dateCreated: Locator;
    readonly responsiblePersons: Locator;
    readonly subAdmin: Locator;
    readonly sensitiveInfo: Locator;

    constructor(page: Page) {
        this.client = page.getByRole("combobox", { name: "Client" });
        this.corpRegNo = page.getByRole("textbox", { name: "Corp reg. no" });
        this.projectTitle = page.getByRole("textbox", {
            name: "Project title",
        });
        this.dateCreated = page.getByRole("textbox", { name: "Date created" });
        this.responsiblePersons = page.getByRole("textbox", {
            name: "Responsible persons",
        });
        this.subAdmin = page.getByRole("combobox", {
            name: "Enter e-mail of person(s) to be added as sub-admin to this list",
        });
        this.sensitiveInfo = page.getByRole("textbox", {
            name: "Sensitive information",
        });
    }

    async fillOrganizationInfo(listName: string, listData: List) {
        await this.client.fill(listData.clientName);
        await this.corpRegNo.fill(listData.corpRegNo);
        await this.projectTitle.fill(listData.listTitle);
        await this.dateCreated.fill(listData.listCreatedDate);
        await this.subAdmin.fill(listData.subAdmin);

        if (listName === "ADVISOR_LOGBOOK" || listName === "ADVISOR_SENSITIVE")
            await this.responsiblePersons.fill(listData.responsible); //IMPORTANT! Not understanding why Market soundings doesn't have responsible as every other list has
        if (listName === "ADVISOR_SENSITIVE")
            await this.sensitiveInfo.fill(listData.aditionalNotes);
    }
}
