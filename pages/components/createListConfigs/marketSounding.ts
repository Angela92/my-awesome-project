import { Page, Locator } from "@playwright/test";
import { List } from "../../../data/listCreationRandomInputs";

export class MarketSounding {
    readonly page: Page;
    readonly nextBtn: Locator;
    readonly transactionType: Locator;
    readonly industry: Locator;
    readonly anonymousDescriptionIssuer: Locator;
    readonly informationSharedWithInvestors: Locator;
    readonly insideInformationDisclosedMS: Locator;
    readonly reasonsAboutDisclosure: Locator;
    readonly estimatedDateDisclosure: Locator;
    readonly estimatedHourDisclosure: Locator;

    constructor(page: Page) {
        this.page = page;
        this.nextBtn = page.getByRole("button", { name: "Next" });
        this.transactionType = page.getByRole("combobox", {
            name: "Transaction type",
        });
        this.industry = page.getByRole("combobox", { name: "Industry" });
        this.anonymousDescriptionIssuer = page.getByRole("textbox", {
            name: "Anonymous description of the issuer, e.g nationality and size",
        });
        this.informationSharedWithInvestors = page.getByRole("textbox", {
            name: "Information that will be shared with investors ",
        });
        this.insideInformationDisclosedMS = page.locator(
            "#isdisclosedinformation",
        );
        this.reasonsAboutDisclosure = page.getByRole("textbox", {
            name: "State the reasons for the conclusion about disclosure of inside information",
        });
        this.estimatedDateDisclosure = page.getByRole("textbox", {
            name: "Estimated time when the information is expected to be made public or the transaction launched ",
        });
        this.estimatedHourDisclosure = page.locator(
            "#dateexpectedpublicTimeValue input",
        );
    }

    async fillTransactionType(option: string) {
        await this.transactionType.click();
        await this.page.getByRole("option", { name: option }).click();
    }

    async fillIndustry(option: string) {
        await this.industry.click();
        await this.page.getByRole("option", { name: option }).click();
    }

    async fillInsideInformationDisclosedMS(option: string) {
        await this.insideInformationDisclosedMS
            .getByRole("radio", { name: option })
            .click();
    }

    async fillMarketSoundingInfo(listData: List) {
        await this.nextBtn.click();
        await this.fillTransactionType(listData.transactionType);
        await this.fillIndustry(listData.industry);
        await this.anonymousDescriptionIssuer.fill(listData.issuerDescription);
        await this.informationSharedWithInvestors.fill(listData.aditionalNotes);
        await this.fillInsideInformationDisclosedMS(
            listData.insideInfoDisclosedMS,
        );
        await this.reasonsAboutDisclosure.fill(listData.aditionalNotes);
        await this.estimatedDateDisclosure.fill(listData.dateDisclosure);
        await this.estimatedHourDisclosure.fill(listData.hourDisclosure);
    }
}
