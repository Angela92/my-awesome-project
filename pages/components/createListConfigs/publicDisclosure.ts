import { Page, Locator } from "@playwright/test";
import { List } from "../../../data/listCreationRandomInputs";

export class PublicDisclosure {
    readonly chkPublicDisclosure: Locator;
    readonly legitimateInterestsPrejudice: Locator;
    readonly legitimateInterestsConcerned: Locator;
    readonly differentInfo: Locator;
    readonly financialObjectives: Locator;
    readonly marketExpectations: Locator;
    readonly ensureConfidentiality: Locator;
    readonly responsibleDelay: Locator;

    constructor(page: Page) {
        this.chkPublicDisclosure = page.getByRole("checkbox", {
            name: "Public disclosure is not being delayed",
        });
        this.legitimateInterestsPrejudice = page.locator(".radio-label", {
            hasText:
                "Is an immediate disclosure likely to prejudice the legitimate interests of the company?",
        });
        this.legitimateInterestsConcerned = page.locator("#legitimateInterest");
        this.differentInfo = page.locator("#deviatingInfo");
        this.financialObjectives = page.locator("#financialGoals");
        this.marketExpectations = page.locator("#marketExpectations");
        this.ensureConfidentiality = page.locator("#ensureConfidentiality");
        this.responsibleDelay = this.responsibleDelay = page.getByRole(
            "textbox",
            { name: "Responsible for the delay" },
        );
    }

    async checkUncheckPublicDisclosures(checked: boolean, listData: List) {
        if (checked === true) {
            await this.chkPublicDisclosure.check();
        } else {
            await this.chkPublicDisclosure.uncheck();
            await this.responsibleDelay.fill(listData.responsible);
        }
    }

    async fillLegitimateInterestPrejudice(option: string) {
        await this.legitimateInterestsPrejudice
            .getByRole("radio", { name: option })
            .click();
    }

    async fillLegitimateInterestConcern(option: string) {
        await this.legitimateInterestsConcerned
            .getByRole("radio", { name: option })
            .click();
    }

    async fillDifferentInfo(option: string) {
        await this.differentInfo.getByRole("radio", { name: option }).click();
    }

    async fillObjectives(option: string) {
        await this.financialObjectives
            .getByRole("radio", { name: option })
            .click();
    }

    async fillMarketExpectations(option: string) {
        await this.marketExpectations
            .getByRole("radio", { name: option })
            .click();
    }

    async fillensureConfidentiality(option: string) {
        await this.ensureConfidentiality
            .getByRole("radio", { name: option })
            .click();
    }

    async fillRadioButtons() {
        await this.fillLegitimateInterestPrejudice("Yes");
        await this.fillLegitimateInterestConcern(
            "The information will be publicly disclosed in an interim report at a date which has been communicated in advance.",
        );
        await this.fillDifferentInfo("No");
        await this.fillObjectives("No");
        await this.fillMarketExpectations("No");
        await this.fillensureConfidentiality("Yes");
    }
}
