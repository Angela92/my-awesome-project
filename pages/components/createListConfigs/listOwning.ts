import { Locator, Page } from "@playwright/test";

export class ListOwning {
    readonly page: Page;
    readonly group: Locator;
    readonly groupOption: Locator;

    constructor(page: Page) {
        this.page = page;
        this.group = page.getByRole("combobox", { name: "Group" });
    }

    async fillGroup(option: string) {
        await this.group.click();
        await this.page.getByRole("option", { name: option }).click();
    }
}
