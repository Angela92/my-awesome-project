import { Page, Locator } from "@playwright/test";
import { PublicDisclosure } from "../createListConfigs/publicDisclosure";
import { OrganizationalView } from "../createListConfigs/organizationalView";
import { MarketSounding } from "../createListConfigs/marketSounding";
import { ListOwning } from "../createListConfigs/listOwning";
import { List } from "../../../data/listCreationRandomInputs";

export type listConfigs = {
    enableListType: boolean;
    enableListToBeClosed: boolean;
    enableForceDisclosures: boolean;
    enableAssociatedEntities: boolean;
    enableSwedbankFieldSet: boolean;
    enableSingleRestriction: boolean;
    enablePermanentNotificationOnListClose: boolean;
    enablePermanentNotificationOnListOpen: boolean;
    enablePreDayRemindersNotification: boolean;
    enableOwnershipSection: boolean;
    enableMemberFormCompletionLink: boolean;
    enablePositionSection: boolean;
    enableCustomScheduledMessages: boolean;
    enableMarketSoundingTeaser: boolean;
    enable2FA: boolean;
    enableOrganizationalView: boolean;
    enableYearlyExportOption: boolean;
    enableToRestrictUsersFromAddingExcludedMS: boolean;
    enableRecordConversationMS: boolean;
    enableLinkLifelimit: boolean;
    listOwning: string;
    enableNotificationsForIncompleteLists: boolean;
};

export class CreateListForm {
    readonly page: Page;
    readonly listTitle: Locator;
    readonly insideInformation: Locator;
    readonly description: Locator;
    readonly listCreatedDate: Locator;
    readonly listCreatedHour: Locator;
    readonly responsibleDelay: Locator;
    readonly responsiblePerson: Locator;
    readonly reminderFrequency: Locator;
    readonly additionalNotes: Locator;
    readonly createListBtn: Locator;
    readonly publicDisclosure: PublicDisclosure;
    readonly organizationalView: OrganizationalView;
    readonly marketSounding: MarketSounding;
    readonly listOwning: ListOwning;

    constructor(page: Page) {
        this.page = page;
        this.listTitle = page.getByRole("textbox", { name: "List title" });
        this.insideInformation = page.getByRole("textbox", {
            name: "Inside information",
        });
        this.description = page.getByRole("textbox", { name: "Description" });
        this.listCreatedDate = page.getByRole("textbox", {
            name: "List created",
        });
        this.listCreatedHour = page.locator("#dateOpenedTimeValue input");
        this.responsiblePerson = page.getByRole("textbox", {
            name: "Responsible person",
        });
        this.reminderFrequency = page.getByRole("textbox", {
            name: "Reminder frequency",
        });
        this.additionalNotes = page.getByRole("textbox", {
            name: "Additional notes",
        });
        this.createListBtn = page.getByRole("button", { name: "Create list" });
        this.publicDisclosure = new PublicDisclosure(page);
        this.organizationalView = new OrganizationalView(page);
        this.marketSounding = new MarketSounding(page);
        this.listOwning = new ListOwning(page);
    }

    async goto(listName: string) {
        await this.page.goto(`/list/${listName}/settings`);
    }

    async fillFormValid(
        listName: string,
        listData: List,
        listConfigs: listConfigs,
    ) {
        await this.listTitle.fill(listData.listTitle);
        await this.listCreatedDate.fill(listData.listCreatedDate);
        await this.listCreatedHour.fill(listData.listCreatedHour);
        await this.reminderFrequency.fill(listData.reminderFrequency);
        await this.additionalNotes.fill(listData.aditionalNotes);

        if (listConfigs.enableForceDisclosures === false)
            await this.responsiblePerson.fill(listData.responsible);

        if (listName.includes("SENSITIVE"))
            await this.description.fill(listData.aditionalNotes); //IMPORTANT! Check if this info can be obtained in content settings

        if (listName.includes("LOGBOOK"))
            await this.insideInformation.fill(listData.aditionalNotes); //IMPORTANT! Check if this info can be obtained in content settings
    }

    async fillPublicDisclosure(checked: boolean, listData: List) {
        await this.publicDisclosure.checkUncheckPublicDisclosures(
            checked,
            listData,
        );
        await this.publicDisclosure.fillRadioButtons();
    }

    async fillFormOrganizationalView(listName: string, listData: List) {
        await this.organizationalView.fillOrganizationInfo(listName, listData);

        if (listName === "ADVISOR_LOGBOOK")
            await this.insideInformation.fill(listData.aditionalNotes); //IMPORTANT! Check if this info can be obtained in content settings

        await this.listCreatedHour.fill(listData.listCreatedHour);
    }

    async createList(
        listConfigs: listConfigs,
        listName: string,
        listData: List,
    ) {
        listConfigs.enableOrganizationalView === false
            ? await this.fillFormValid(listName, listData, listConfigs)
            : await this.fillFormOrganizationalView(listName, listData);

        if (listConfigs.listOwning === "GROUP")
            await this.listOwning.fillGroup("MAIN_ADMIN_GROUP");

        if (listConfigs.enableForceDisclosures === true)
            await this.fillPublicDisclosure(false, listData);

        if (listConfigs.enableMarketSoundingTeaser === true)
            await this.marketSounding.fillMarketSoundingInfo(listData);

        this.createListBtn.click();
    }
}
