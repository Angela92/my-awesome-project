import { Page, Locator } from "@playwright/test";

export class ListPage {
    readonly page: Page;
    readonly newListBtn: Locator;
    readonly openBtn: Locator;

    constructor(page: Page) {
        this.page = page;
        this.newListBtn = page.getByRole("button", { name: "New list" });
        this.openBtn = page.getByRole("button", { name: "Open" });
    }

    async goto(listName: string) {
        await this.page.goto(`/list/${listName}/status/active`);
    }
}
