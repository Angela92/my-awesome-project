import { test } from "@playwright/test";
import listConfig from "../listConfig.json";
import { createRandomList } from "../data/listCreationRandomInputs";
import { CreateListSteps } from "../steps/createList";

const listNames = Object.keys(listConfig);

test("create new logbook list", async ({ page }) => {
    const createListSteps = new CreateListSteps(page);
    const newList = createRandomList();

    /*GIVEN*/ await createListSteps.userNavigatesToListPage(listNames[0]);
    /*WHEN*/ await createListSteps.userCreatesAList(
        listConfig.LOGBOOK,
        listNames[0],
        newList,
    );
    /*THEN*/ await createListSteps.expectListToBeCreatedWithCorrectInfo(
        newList,
        listNames[0],
    );
});

test("create new advisor logbook list", async ({ page }) => {
    const createListSteps = new CreateListSteps(page);
    const newList = createRandomList();

    /*GIVEN*/ await createListSteps.userNavigatesToListPage(listNames[1]);
    /*WHEN*/ await createListSteps.userCreatesAList(
        listConfig.ADVISOR_LOGBOOK,
        listNames[1],
        newList,
    );
    /*THEN*/ await createListSteps.expectListToBeCreatedWithCorrectInfo(
        newList,
        listNames[1],
    );
});

test("create market sounding list", async ({ page }) => {
    const createListSteps = new CreateListSteps(page);
    const newList = createRandomList();

    /*GIVEN*/ await createListSteps.userNavigatesToListPage(listNames[2]);
    /*WHEN*/ await createListSteps.userCreatesAList(
        listConfig.MARKET_SOUNDING,
        listNames[2],
        newList,
    );
    /*THEN*/ await createListSteps.expectListToBeCreatedWithCorrectInfo(
        newList,
        listNames[2],
    );
});

test("create sensitive", async ({ page }) => {
    const createListSteps = new CreateListSteps(page);
    const newList = createRandomList();

    /*GIVEN*/ await createListSteps.userNavigatesToListPage(listNames[3]);
    /*WHEN*/ await createListSteps.userCreatesAList(
        listConfig.SENSITIVES,
        listNames[3],
        newList,
    );
    /*THEN*/ await createListSteps.expectListToBeCreatedWithCorrectInfo(
        newList,
        listNames[3],
    );
});

test("create advisor sensitive", async ({ page }) => {
    const createListSteps = new CreateListSteps(page);
    const newList = createRandomList();

    /*GIVEN*/ await createListSteps.userNavigatesToListPage(listNames[4]);
    /*WHEN*/  await createListSteps.userCreatesAList(
        listConfig.ADVISOR_SENSITIVE,
        listNames[4],
        newList,
    );
    /*THEN*/ await createListSteps.expectListToBeCreatedWithCorrectInfo(
        newList,
        listNames[4],
    );
});

test("Create custom", async ({ page }) => {
    const createListSteps = new CreateListSteps(page);
    const newList = createRandomList();

    /*GIVEN*/ await createListSteps.userNavigatesToListPage(listNames[5]);
    /*WHEN*/ await createListSteps.userCreatesAList(
        listConfig.CUSTOM,
        listNames[5],
        newList,
    );
    /*THEN*/ await createListSteps.expectListToBeCreatedWithCorrectInfo(
        newList,
        listNames[5],
    );
});

