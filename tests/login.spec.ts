// import { expect, test } from "@playwright/test"
// import { LoginPage } from "../pages/loginPage"

// test("login with success", async ({ page }) => {
//     const loginPage = new LoginPage(page);

//     /*GIVEN*/   await loginPage.goto();
//     /*GIVEN*/   await expect(page.locator('p')).toContainText("Login with your credentials");
//     /*WHEN*/    await loginPage.login(page, "qa_auto_1@insiderlog.mailosaur.net", "Euronext@123", "123");
//     /*THEN*/    await expect(page).toHaveURL('/home');
// })

// test("login with wrong pass", async ({ page }) => {
//     const loginPage = new LoginPage(page);

//     /*GIVEN*/   await loginPage.goto();
//     /*WHEN*/    await loginPage.wrongLogin("qa_auto_1@insiderlog.mailosaur.net", "12345");
//     /*THEN*/    await expect(loginPage.alert).toHaveText('The credentials are invalid');
// })

// test("forgot pass", async ({ page }) => {
//     const loginPage = new LoginPage(page);

//     /*GIVEN*/   await loginPage.goto();
//     /*WHEN*/    await loginPage.forgotPassword('')
//     /*THEN*/    await expect(loginPage.alert).toHaveText("If the email is valid you should have received a recovery email")
// })
