import { Browser, chromium, expect } from "@playwright/test";

async function globalSetup() {
    const browser: Browser = await chromium.launch();
    const context = await browser.newContext();
    const page = await context.newPage();

    await page.goto("https://autotest1.qa.insider-log.com");
    await page
        .getByRole("textbox", { name: "Enter User Name/ Email" })
        .fill("qa_auto_1@insiderlog.mailosaur.net");
    await page
        .getByRole("textbox", { name: "Enter your password" })
        .fill("Euronext@123");
    await page.getByRole("button", { name: "Login" }).click();
    await page.getByRole("textbox", { name: "Enter sms pincode" }).fill("123");
    await page.getByRole("button", { name: "Login" }).click();
    await expect(page).toHaveURL("https://autotest1.qa.insider-log.com/home");
    expect(page.getByRole("link", { name: "Start" })).toBeVisible({
        timeout: 30000,
    });

    //Save the state - means we are logged in

    await page.context().storageState({ path: "./.auth/user.json" });

    await browser.close();
}

export default globalSetup;
